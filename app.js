"use strict";

var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');
var util = require('util');
var handlebars = require('handlebars');
var jsdom = require("jsdom");
var window = jsdom.jsdom().defaultView;
var fetch = require('node-fetch');
var htmlModules = require('./lib/htmlModules');
var dataminist = require('./lib/danimist');
var ec = require('./lib/ecoApi');
var googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyBeFU14LSH20q7Jv8nNC7wHRd0usTTm51E'
});

var coordinates = {};

app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/foundation', express.static(__dirname + '/node_modules/foundation-sites/dist/'));
app.use('/javascripts', express.static(__dirname + '/public/javascripts/'));
app.use('/stylesheets', express.static(__dirname + '/public/stylesheets/'));
app.use('/img', express.static(__dirname + '/public/img/'));
app.use('/lib', express.static(__dirname + '/lib/'));

app.get('/', function (req, res) {
    res.sendfile('./public/index.html');
});


io.on('connection', function (socket) {

    htmlModules.getLanding(socket);
    htmlModules.getFooter(socket);


    socket.on('getLanding', function(msg) {
        htmlModules.getLanding(socket);
    });

    console.log('a user connected');
    socket.on('disconnect', function () {
        console.log('a user disconnected');
    });

    socket.on('getMap', function(msg){
        htmlModules.getMap(socket);
        /*if (coordinates.length === 0) {
            dataminist.getNewPoints(socket, coordinates);
            ec.getDevices(socket);
        } else {
            dataminist.getNewPoints(socket, coordinates);
            console.log("CACHED");
            ec.getDevices(socket);
        }*/
    });

});

http.listen(80, function () {
    console.log('listening on *:80');
});