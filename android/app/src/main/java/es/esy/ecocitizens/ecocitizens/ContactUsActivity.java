package es.esy.ecocitizens.ecocitizens;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import static android.R.id.input;
import static es.esy.ecocitizens.ecocitizens.R.id.input_text;

public class ContactUsActivity extends AppCompatActivity {

    private EditText inputSubject, inputText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        inputSubject = (EditText) findViewById(R.id.input_subject);
        inputText = (EditText) findViewById(R.id.input_text);

        findViewById(R.id.buttonSendMail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"ecocitizens.ua@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, inputSubject.getText().toString());
                i.putExtra(Intent.EXTRA_TEXT, inputText.getText().toString());
                try {
                    startActivity(Intent.createChooser(i, "Відправка повідомлення..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(ContactUsActivity.this, "Не налаштований поштовий клієнт.", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public void onClickBottomMenu(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.btnPhone:
                intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "+380 93 385 0579", null));
                break;
            case R.id.btnFB:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.facebook.com/ecocitizens"));
                break;
            case R.id.btnVK:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://vk.com/ecocitizens"));
                break;
            case R.id.btnGPlus:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://plus.google.com/u/0/104339487019946657125"));
                break;

        }
        if (intent != null)
            startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
