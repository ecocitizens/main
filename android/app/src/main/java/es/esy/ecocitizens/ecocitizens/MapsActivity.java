package es.esy.ecocitizens.ecocitizens;

import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmapsextensions.ClusteringSettings;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.OnMapReadyCallback;
import com.androidmapsextensions.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.androidmapsextensions.Marker;
import com.androidmapsextensions.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private final static String TAG = "MapsActivity";
    private static final String MAIN_MARKER_NAME = "TEST";
    private GoogleMap mMap;
    private BottomSheetBehavior mBottomSheetBehavior;
    private TextView textViewSensorInfoTitle, textViewSensorInfoTemperature, textViewSensorInfoVologist, textViewSensorInfoCO2, textViewSensorInfoDust, textViewSensorInfoPressure, textViewSensorInfoLight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        View bottomSheet = findViewById(R.id.bottom_sheet);

        textViewSensorInfoTitle = (TextView) findViewById(R.id.sensor_info_title);
        textViewSensorInfoTemperature = (TextView) findViewById(R.id.sensor_info_temperature);
        textViewSensorInfoVologist = (TextView) findViewById(R.id.sensor_info_vologist);
        textViewSensorInfoCO2 = (TextView) findViewById(R.id.sensor_info_CO2);
        textViewSensorInfoDust = (TextView) findViewById(R.id.sensor_info_dust);
        textViewSensorInfoPressure = (TextView) findViewById(R.id.sensor_info_pressure);
        textViewSensorInfoLight = (TextView) findViewById(R.id.sensor_info_light);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getExtendedMapAsync(this);

        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight(0);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    mBottomSheetBehavior.setPeekHeight(0);
                }
            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        else
            super.onBackPressed();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        ClusteringSettings clusteringSettings = new ClusteringSettings();
        clusteringSettings.addMarkersDynamically(true);
        mMap.setClustering(clusteringSettings);


        LatLng VINNYTSIA = new LatLng(50.476589, 30.507959);

        mMap.addMarker(new MarkerOptions().position(VINNYTSIA).title(MAIN_MARKER_NAME).clusterGroup(777).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));


        String[] name = getResources().getStringArray(R.array.name);
        String[] lng = getResources().getStringArray(R.array.lng);
        String[] lat = getResources().getStringArray(R.array.lat);
        for (int i = 0; i < lng.length; i++) {
            addMarker(new LatLng(Float.valueOf(lat[i]), Float.valueOf(lng[i])), name[i]);
        }

        LatLngBounds UKRAINE = new LatLngBounds(
                new LatLng(44.4f, 21.6f), new LatLng(52.3f, 40.3f));


        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(UKRAINE, 0));
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                if (marker.getTitle()!=null && marker.getTitle().equals(MAIN_MARKER_NAME)) {
                    setSensorInfo(new String[]{"Sensor1", "21 C", "null", "921", "null", "764 мм/рт.ст.", "null"});
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 10));
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    return true;
                } else {
                    if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    Toast.makeText(MapsActivity.this, "У даному місці датчик датчик не встановлено", Toast.LENGTH_LONG).show();
                    return false;
                }
            }
        });
    }

    private void addMarker(LatLng position, String title) {
        mMap.addMarker(new MarkerOptions().position(position).title(title));
    }

    private void setSensorInfo(String[] arrayData) {
        textViewSensorInfoTitle.setText(arrayData[0]);
        textViewSensorInfoTemperature.setText("Температура: " + arrayData[1]);
        textViewSensorInfoVologist.setText("Вологість: " + arrayData[2]);
        textViewSensorInfoCO2.setText("СО2: " + arrayData[3]);
        textViewSensorInfoDust.setText("Пил: " + arrayData[4]);
        textViewSensorInfoPressure.setText("Атмосферний тиск: " + arrayData[5]);
        textViewSensorInfoLight.setText("Освітлення: " + arrayData[6]);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
