/**
 * Created by Hetfield on 18.11.2016.
 */
var socket = io();
var realMarkers = {};
var gMarkers = {};
gMarkers['pollutions'] = [];
realMarkers['pollutions'] = [];
gMarkers['devices'] = [];
realMarkers['devices'] = [];

socket.on("mapInit", function (msg) {

    $('#body').empty();
    $('#body').append(msg);

});

socket.on("getLanding", function (msg) {

    $('#body').empty();
    $('#body').append(msg);

});

socket.on("pollution", function (msg) {
    var pollution = JSON.parse(msg);
    console.log(pollution.location.lat);
    gMarkers.pollutions.push(pollution);

    var localP = map.addMarker({
        lat: pollution.location.lat,
        lng: pollution.location.lng,
        title: pollution.title,
        click: function(e) {
            alert(pollution.title);
        }
    });
    realMarkers.pollutions.push(localP);
    //console.log(localP);
});

socket.on("device", function (msg) {
    var device = JSON.parse(msg);
    gMarkers.devices.push(device);
    console.log("device");
    var localD = map.addMarker({
        lat: device.location.lat,
        lng: device.location.lng,
        title: device.title,
        click: function(e) {
            alert(device.title);
        }
    });
    realMarkers.devices.push(localD);
});


socket.on("getFooter", function (msg) {
    $('#foot').empty();
    $('#foot').append(msg);
});

socket.on("getMap", function (msg) {
    //console.log(msg);
    $('#body').empty();
    $('#body').append(msg);
    $("#map").height();
});
