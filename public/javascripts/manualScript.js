/**
 * Created by Маринюк on 11/19/2016.
 */
$('a[href^="#"]').bind('click.smoothscroll', function (e) {
    e.preventDefault();

    var target = this.hash,
        $target = $(target);

    $('html, body').stop().animate({
        'scrollTop': $target.offset().top
    }, 500, 'swing', function () {
        window.location.hash = target;
    });
});

$(document).ready(function () {

    console.log("Doc is rdy");
    $('#body').on('click', '#menu_btn_1', function () {
        console.log('getMap to server');
        socket.emit('getMap', '');
    });
    $('#body').on('click', '#menu_btn_0', function () {
        socket.emit('getLanding', '');
    });
    $('#body').on('click', '.title-bar .menu-icon', function () {
        $(".top-bar").toggle();
    });
    $('#body').on('click', '.clearPollutions', function () {
        $(this).hide();
        $('.addPollutions').css('display','inline-block');
        console.log(map);
        console.log(realMarkers.pollutions);

        for (id in realMarkers.pollutions) {
            map.removeMarker(realMarkers.pollutions[id]);
        }
    });
    $('#body').on('click', '.addPollutions', function () {
        $(this).hide();
        $('.clearPollutions').css('display','inline-block');
        console.log(gMarkers.pollutions);
        var pollution;
        for (var id in gMarkers.pollutions) {
            var pollution = gMarkers.pollutions[id];
            var localP = map.addMarker({
                lat: pollution.location.lat,
                lng: pollution.location.lng,
                title: pollution.title,
                click: function(e) {
                    alert(pollution.title);
                }
            });
            realMarkers.pollutions[id] = localP;
        }
    });
    $('#body').on('click', '.clearDevices', function () {
        $(this).hide();
        $('.addDevices').css('display','inline-block');
        console.log(map);
        console.log(realMarkers.devices);

        for (id in realMarkers.devices) {
            map.removeMarker(realMarkers.devices[id]);
        }
    });
    $('#body').on('click', '.addDevices', function () {
        $(this).hide();
        $('.clearDevices').css('display','inline-block');
        console.log(gMarkers.devices);
        var device;
        for (var id in gMarkers.devices) {
            var device = gMarkers.devices[id];
            var localD = map.addMarker({
                lat: device.location.lat,
                lng: device.location.lng,
                title: device.title,
                click: function(e) {
                    alert(device.title);
                }
            });
            realMarkers.devices[id] = localD;
        }
    });
});

