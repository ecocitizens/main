/**
 * Created by Маринюк on 11/18/2016.
 */

"use strict";

var fs = require('fs');
var dataminist = require('../lib/danimist');
var ec = require('../lib/ecoApi');


exports.getLanding = function (socket) {
    fs.readFile('./public/modules/landing.html', 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        socket.emit("getLanding", data);
    });
};

exports.getFooter = function (socket) {
    fs.readFile('./public/modules/footer.html', 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        socket.emit("getFooter", data);
    });
};

exports.getMap = function (socket) {
    fs.readFile('./public/modules/map2.html', 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        socket.emit("getMap", data);
        dataminist.getNewPoints(socket);
        ec.getDevices(socket);
    });
};
