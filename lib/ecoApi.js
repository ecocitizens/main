/**
 * Created by Маринюк on 11/20/2016.
 */

"use strict";

var jsdom = require("jsdom");
var fetch = require('node-fetch');
var window = jsdom.jsdom().defaultView;
var googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyBeFU14LSH20q7Jv8nNC7wHRd0usTTm51E'
});

exports.getDevices = function (socket) {
    var devices = {title : "Device", location: {lat : "49.248134", lng : "28.492190"}};
    socket.emit("device", JSON.stringify(devices));
};