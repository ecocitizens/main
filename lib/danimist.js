/**
 * Created by Маринюк on 11/20/2016.
 */

"use strict";

var jsdom = require("jsdom");
var fetch = require('node-fetch');
var window = jsdom.jsdom().defaultView;
var googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyBeFU14LSH20q7Jv8nNC7wHRd0usTTm51E'
});

var coordinates = {};

exports.getNewPoints = function (socket) {

    fetch('https://data.danimist.org.ua/api/action/datastore_search?resource_id=1dddaae3-bfa0-43c7-becd-69a33782c5ce&limit=100').then(function (res) {
        return res.text();
    }).then(function (body) {
        jsdom.jQueryify(window, "https://code.jquery.com/jquery-3.1.1.js", function () {
            var $ = window.$;
            var tableInJSON = JSON.parse(body).result.records;
            tableInJSON.forEach(function (item) {
                googleMapsClient.geocode({
                    address: item.address
                }, function (err, response) {
                    if (!err) {
                        coordinates.title = item.title;
                        coordinates.emission_in_air = item.emission_in_air;
                        coordinates.emission_into_water = item.emission_into_water;
                        coordinates.generated_waste = item.generated_waste;
                        coordinates.location = response.json.results[0].geometry.location;
                        socket.emit("pollution", JSON.stringify(coordinates));
                    } else {
                        console.log(err);
                    }
                });
            }, function () {
                console.log("Complete");
            });
        });
    });
};